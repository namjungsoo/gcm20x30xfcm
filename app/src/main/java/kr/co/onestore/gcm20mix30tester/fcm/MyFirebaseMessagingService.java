package kr.co.onestore.gcm20mix30tester.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import kr.co.onestore.gcm20mix30tester.MainActivity;
import kr.co.onestore.gcm20mix30tester.R;

/*
package com.onestore.logintest

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService2 : FirebaseMessagingService() {
    // 이 함수는 BG에서는 자체적으로 노티를 띠워주며 FG시에만 여기서 처리된다.
    override fun onMessageReceived(var1: RemoteMessage?) {
        Log.e("Jungsoo", "onMessageReceived=" + var1!!.toString())

        val extras = Bundle()
        var1.data.forEach({
            extras.putString(it.key, it.value)
        })
//        extras.putString("message", var1!!.data.get("message"))
//        extras.putString("title", var1!!.data.get("title"))
        sendNotification(extras)
    }

    // 상태바에 공지
    private fun sendNotification(extras: Bundle?) {
        Log.e("Jungsoo", "sendNotification")

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val intent = Intent(this, MainActivity::class.java)
        intent.replaceExtras(extras)
        Log.e("Jungsoo", "sendNotification intent size=$intent ${intent?.extras}")

        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val content = extras?.getString("message")
        var title = extras?.getString("title")    @Override
    public void onMessageReceived(RemoteMessage var1) {
        Log.e("Jungsoo", "onMessageReceived=" + var1.toString());

        Bundle extras = new Bundle();
        for(String key : var1.getData().keySet()) {
            String value = var1.getData().get(key);
            extras.putString(key, value);
        }
        sendNotification(extras);
    }


//        if(title == null)
//            title = "(null)"

        val builder = NotificationCompat.Builder(this, "channel_id")
        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(content)
                .setContentTitle(title)
                .setContentIntent(pendingIntent)

        notificationManager.notify(1, builder.build())
    }

}
 */


// 지금은 사용하지 않고 kotlin class에서 처리한다.
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public void onMessageReceived(RemoteMessage var1) {
        Log.e("Jungsoo", "onMessageReceived=" + var1.toString());

        Bundle extras = new Bundle();
        for (String key : var1.getData().keySet()) {
            String value = var1.getData().get(key);
            extras.putString(key, value);
            Log.e("Jungsoo", "key=" + key + " value=" + value);
        }

//        sendNotification(extras);
    }

    // 상태바에 공지
    private void sendNotification(Bundle extras) {
        Log.e("Jungsoo", "sendNotification");

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent = new Intent(this, MainActivity.class);
        intent.replaceExtras(extras);
        Log.e("Jungsoo", "sendNotification intent=" + intent + " extras=" + intent.getExtras());

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        String content = extras.getString("message");
        String title = extras.getString("title");

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "channel_id");
        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(content)
                .setContentTitle(title)
                .setContentIntent(pendingIntent);

        notificationManager.notify(1, builder.build());
    }
}
