package kr.co.onestore.gcm20mix30tester;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.IOException;

import kr.co.onestore.gcm20mix30tester.gcm30.RegistrationIntentService;

public class MainActivity extends AppCompatActivity {

    final static String TAG = MainActivity.class.getSimpleName();
    final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    /**
     * Instance ID를 이용하여 디바이스 토큰을 가져오는 RegistrationIntentService를 실행한다.
     */
    public void getToken30() {
        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    //String sender_id = "630997358215,364152695002";
    //String sender_id = "950981591076";// api-project
    String sender_id = "630997358215";// tstore QA

    public void getToken20() {

        Intent registrationIntent = new Intent("com.google.android.c2dm.intent.REGISTER");
        registrationIntent.setPackage("com.google.android.gsf");
        registrationIntent.putExtra("sender", sender_id);
        registrationIntent.putExtra("app", PendingIntent.getBroadcast(this, 0, new Intent(), 0));
        //this.startService(registrationIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.e("Jungsoo", "startForegroundService");
            this.startForegroundService(registrationIntent);
        } else {
            Log.e("Jungsoo", "startService");
            this.startService(registrationIntent);
        }
    }

    public void getTokenFCM() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String token = FirebaseInstanceId.getInstance().getToken(sender_id, "FCM");
                    Log.e("Jungsoo", "tokenFCM=" + token);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseMessaging.getInstance().setAutoInitEnabled(false);

        //GCM20
        getToken20();

        //GCM30
        getToken30();

        //FCM
        getTokenFCM();
    }

    /**
     * Google Play Service를 사용할 수 있는 환경이지를 체크한다.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}
