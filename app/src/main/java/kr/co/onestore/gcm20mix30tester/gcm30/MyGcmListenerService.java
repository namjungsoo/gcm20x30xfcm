package kr.co.onestore.gcm20mix30tester.gcm30;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

public class MyGcmListenerService extends GcmListenerService {
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        String title = data.getString("title");
        Log.e("Jungsoo","title=" + title + " message="+message + " from="+from);
    }
}
