package kr.co.onestore.gcm20mix30tester.gcm30;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

public class RegistrationIntentService extends IntentService {
    private static String TAG = RegistrationIntentService.class.getSimpleName();

    public RegistrationIntentService() {
        super(TAG);
    }

    //String senderId = "630997358215,364152695002";
    //String senderId = "950981591076";// api-project
    String senderId = "630997358215";// tstore QA

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        InstanceID instanceID = InstanceID.getInstance(this);
        try {
            String token = instanceID.getToken(senderId,
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Log.e("Jungsoo", "token3.0=" + token);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
